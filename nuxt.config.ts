import defaultMeta from './config/defaultMeta'
const version = process.env.npm_package_version

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'Baze Nuxt 3',
      meta: defaultMeta,
      link: [
        { rel: 'apple-touch-icon', href: '/apple-icon.png' },
        { rel: 'icon', type: 'image/png', href: '/favicon.png' }
      ]
    }
  },

  ssr: false,

  imports: {
    dirs: ['composables', 'composables/**']
  },

  runtimeConfig: {
    API_URL: process.env.API_URL,
    RECAPTCHA_SITE_KEY: process.env.RECAPTCHA_SITE_KEY,
    public: {
      BASE_URL: process.env.NUXT_PUBLIC_SITE_URL
    }
  },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },

  css: [
    '~/assets/scss/main.scss',
    '~/assets/scss/modules/_modules.scss',
    '@vuepic/vue-datepicker/dist/main.css',
    'vue-select/dist/vue-select.css'
  ],

  plugins: [
    { src: '~/plugins/vue-select.js' },
    { src: '~/plugins/recaptcha.js' }
  ],

  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxt/image',
    '@vueuse/nuxt',
    '@pinia/nuxt',
    'nuxt-jsonld',
    // 'nuxt-swiper',
    'nuxt-icons',
    '@nuxtjs/eslint-module',
    './modules/auto-import-eslint.ts',
    '@nuxtjs/i18n',
    'nuxt-simple-sitemap',
    'nuxt-api-party',
    '@nuxtjs/google-fonts'
  ],

  pinia: {
    autoImports: [
      // automatically imports `defineStore`
      'defineStore'
    ]
  },

  i18n: {
    strategy: 'prefix',
    defaultLocale: 'id',
    locales: [
      {
        code: 'id',
        iso: 'id-ID'
      },
      {
        code: 'en',
        iso: 'en-US'
      }
    ],
    vueI18n: './lang/localeConfig.js',
    detectBrowserLanguage: false
  },

  googleFonts: {
    families: {
      Roboto: true,
      Montserrat: true
    }
  },

  build: {
    transpile: ['@vuepic/vue-datepicker']
  },

  image: {
    format: ['webp']
  },

  // This is the amount of milliseconds to cache the sitemap for.
  sitemap: {
    defaults: {
      changefreq: 'daily',
      priority: 0.8,
      lastmod: new Date()
    },
    cacheTtl: 1000 * 60 * 60 * 24 // 1 day
  },

  apiParty: {
    endpoints: {
      suitapi: {
        url: process.env.API_URL!,
        token: `Bearer ${process.env.API_TOKEN!}`,
        headers: {
          accept: 'application/json',
          Authorization: `Bearer ${process.env.API_TOKEN!}`
        }
      }
    }
  },

  vite: {
    plugins: [
      // sentryVitePlugin({
      //   url: process.env.SENTRY_DSN,
      //   org: 'suitmedia',
      //   project: 'suitmedia-frontend'
      // })
    ],
    optimizeDeps: { exclude: ['fsevents'] },

    build: {
      rollupOptions: {
        output: {
          entryFileNames: `assets/[hash]-${version}.js`,
          chunkFileNames: `assets/[hash]-${version}.js`,
          assetFileNames: assetInfo => {
            if (assetInfo?.name?.endsWith('.css'))
              return `assets/css/[hash]-${version}[extname]`
            if (/\.(png|jpe?g|gif|svg|webp)$/i.test(assetInfo?.name || ''))
              return `assets/img/[hash]-${version}[extname]`
            if (/\.(woff|woff2|eot|ttf|otf)$/i.test(assetInfo?.name || ''))
              return `assets/fonts/[hash]-${version}[extname]`
            if (/\.(mp4|webm|ogv)$/i.test(assetInfo?.name || ''))
              return `assets/videos/[hash]-${version}[extname]`
            return `assets/[hash]-${version}[extname]`
          }
        }
      }
    },

    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "~/assets/scss/partials/_variables.scss" as *;'
        }
      }
    }
  }
})
