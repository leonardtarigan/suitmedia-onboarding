import menus from '@/data/menus.json'

export default defineEventHandler(() => {
  return menus
})
